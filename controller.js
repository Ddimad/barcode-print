function generateBarcode(mac, snp, currentDate){

  // settings to barcode lib
  var settings = {
    output:"css",
    bgColor: $("#bgColor").val(),
    color: $("#color").val(),
    barWidth: $("#barWidth").val(),
    barHeight: $("#barHeight").val(),
    moduleSize: $("#moduleSize").val(),
    posX: $("#posX").val(),
    posY: $("#posY").val(),
    addQuietZone: $("#quietZoneSize").val()
  };

  // show the barcode
  $("#canvasTarget").hide();
  $("#barcodeTarget").html("").show().barcode(mac + " SNP:" + snp, "datamatrix", settings);

  showDate(currentDate);
  hideButton();
}

function hideButton() {
  var btnGenerate = document.getElementById("submit");
  btnGenerate.style.display = "none";
}

function showDate(currentDate) {
  var pDate = document.getElementById("date");
  pDate.textContent = currentDate;
}

$(function(){
  $('input[name=btype]').click(function(){
    if ($(this).attr('id') == 'datamatrix') showConfig2D(); else showConfig1D();
  });

});
  
